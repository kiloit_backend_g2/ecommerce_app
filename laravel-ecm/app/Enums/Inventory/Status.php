<?php

namespace App\Enums\Inventory;

use App\Enums\BaseEnum;

final class Status extends BaseEnum
{
    const InStock = 0;
    const OutOfStock = 1;
    const LowStock = 2;
}
