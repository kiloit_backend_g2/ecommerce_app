<?php

namespace App\Enums\Order;

use App\Enums\BaseEnum;

final class Status extends BaseEnum
{
    const Pending = 0;
    const Processing = 1;
    const Shipped = 2;
    const Rejected = 3;
    const Completed = 4;
}
