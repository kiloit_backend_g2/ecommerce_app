<?php

namespace App\Enums\Transaction;

use App\Enums\BaseEnum;

class ResponseMessage extends BaseEnum
{
    const Pending = 0;
    const Approved = 1;
    const Declined = 2;
    const Error = 3;
}
