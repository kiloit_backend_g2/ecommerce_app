<?php

namespace App\Enums\Transaction;

use App\Enums\BaseEnum;

final class Status extends BaseEnum
{
    const Pending = 0;
    const Processing = 1;
    const Completed = 2;
    const Failed = 3;
    const Canceled = 4;
}
