<?php

namespace App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Exceptions\InvalidEnumMemberException;
use Illuminate\Support\Arr;

class BaseEnum extends Enum
{
    /**
     * Construct an Enum instance.
     *
     * @param  TValue  $enumValue
     *
     * @catch \BenSampo\Enum\Exceptions\InvalidEnumMemberException
     */
    public function __construct(mixed $enumValue)
    {
        try {
            parent::__construct($enumValue);
        } catch (InvalidEnumMemberException $e) {
            $this->value = $enumValue;
            $this->key = $enumValue;
            $this->description = '';
        }
    }

    public static function toOptions($keys = []): array
    {
        $selects = parent::asSelectArray();

        if ($keys) {
            $selects = Arr::only($selects, $keys);
        }

        $options = [];
        foreach ($selects as $key => $value) {
            $options[] = [
                'id' => $key,
                'text' => $value,
            ];
        }

        return $options;
    }
}
