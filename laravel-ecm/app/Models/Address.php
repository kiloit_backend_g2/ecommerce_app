<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;
    protected $fillable = [
        'code',
        'name',
        'description',
        'type',
        'parent_code',
        'reference',
        'official_note',
        'note_by_checker',
        'user_id',
    ];
    protected $casts = [
        'code' => 'string',
        'name' => 'string',
        'description' => 'string',
        'type' => 'string',
        'parent_code' => 'string',
        'reference' => 'string',
        'official_note' => 'string',
        'note_by_checker' => 'string',
        'user_id' => 'integer',
    ];
    protected array $translatable = [
        'description',
        'reference',
        'official_note',
        'note_by_checker',
    ];
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
