<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static inRandomOrder()
 */
class StatusCatalog extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
    ];
    protected $casts = [
        'name' => 'string',
    ];
    protected array $translatable = [];
    public function shipmentStatuses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ShipmentStatus::class);
    }
}
