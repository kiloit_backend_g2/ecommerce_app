<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static inRandomOrder()
 */
class Store extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'name',
        'phone',
        'email',
        'street',
        'city',
        'state',
        'zip_code',
    ];
    protected $casts = [
        'name' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'street' => 'string',
        'city' => 'string',
        'state' => 'string',
        'zip_code' => 'string',
    ];
    protected array $translatable = [];
    public function shipments(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Shipment::class);
    }
    public function inventories(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Inventory::class);
    }
}
