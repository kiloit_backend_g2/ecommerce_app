<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static inRandomOrder()
 */
class Brand extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'name'
    ];
    protected $casts = [];
    protected array $translatable = [];

    public function products(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Product::class);
    }
    public function categoryBrands(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CategoryBrand::class);
    }
}
