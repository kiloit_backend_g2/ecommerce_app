<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = [
        'sub_quantity',
        'sub_amount',
        'discount',
        'product_id',
        'order_id',
    ];
    protected $casts = [
        'sub_quantity' => 'integer',
        'sub_amount' => 'decimal:10,2',
        'discount' => 'decimal:4,2',
        'product_id' => 'integer',
        'order_id' => 'integer',
    ];
    protected array $translatable = [];
    public function order(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
    public function product(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
