<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static inRandomOrder()
 */
class Category extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'name',
        'description',
    ];
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
    ];
    protected array $translatable = [
        'description',
    ];
    public function supplierCategories(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(SupplierCategory::class);
    }
    public function categoryBrands(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(CategoryBrand::class);
    }
    public function products(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Product::class);
    }
}
