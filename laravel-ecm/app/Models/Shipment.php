<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static inRandomOrder()
 */
class Shipment extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'store_id',
        'shipment_type_id',
        'payment_type_id',
        'shipping_address',
        'billing_address',
        'product_price',
        'delivery_cost',
        'discount',
        'final_amount',
        'user_id',
    ];
    protected $casts = [
        'store_id' => 'integer',
        'shipment_type_id' => 'integer',
        'payment_type_id' => 'integer',
        'shipping_address' => 'string',
        'billing_address' => 'string',
        'product_price' => 'decimal:8,2',
        'delivery_cost' => 'decimal:8,2',
        'discount' => 'decimal:4,2',
        'final_amount' => 'decimal:8,2',
        'user_id' => 'integer',
    ];
    protected array $translatable = [];
    public function shipmentDetail(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ShipmentDetail::class);
    }
    public function paymentDetail(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PaymentDetail::class);
    }
    public function shipmentStatus(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ShipmentStatus::class);
    }
    public function store(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Store::class);
    }
    public function paymentType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(PaymentType::class);
    }
    public function shipmentType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ShipmentType::class);
    }
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
