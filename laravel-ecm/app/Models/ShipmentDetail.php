<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShipmentDetail extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'shipment_id',
        'product_id',
        'quantity',
        'payment_per_unit',
        'amount',
    ];
    protected $casts = [
        'shipment_id' => 'integer',
        'product_id' => 'integer',
        'quantity' => 'integer',
        'payment_per_unit' => 'decimal:8,2',
        'amount' => 'decimal:8,2',
    ];
    protected array $translatable = [];
    public function product(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
    public function shipment(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Shipment::class);
    }
}
