<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static inRandomOrder()
 */
class Transaction extends Model
{
    use HasFactory;
    protected $fillable = [
        'payment_type_id',
        'date',
        'total_amount',
        'status',
        'response_message',
    ];
    protected $casts = [
        'payment_type_id' => 'integer',
        'date' => 'datetime:Y-m-d H:i:s',
        'total_amount' => 'decimal:10,2',
        'status' => 'integer',
        'response_message' => 'integer',
    ];
    protected array $translatable = [];
    public function paymentDetails(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PaymentDetail::class);
    }
    public function paymentType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(PaymentType::class);
    }
}
