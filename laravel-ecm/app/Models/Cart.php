<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = [
        'quantity',
        'unit_price',
        'user_id',
        'product_id',
    ];
    protected $casts = [
        'quantity' => 'integer',
        'unit_price' => 'decimal:10,2',
        'user_id' => 'integer',
        'product_id' => 'integer',
    ];
    protected array $translatable = [];

    public function product(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
