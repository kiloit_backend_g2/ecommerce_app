<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static inRandomOrder()
 */
class Supplier extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'company_name',
        'contact_name',
        'contact_title',
        'contact_email',
        'address',
        'city',
        'state',
        'country',
        'phone',
    ];
    protected $casts = [
        'company_name' => 'string',
        'contact_name' => 'string',
        'contact_title' => 'string',
        'contact_email' => 'string',
        'address' => 'string',
        'city' => 'string',
        'state' => 'string',
        'country' => 'string',
        'phone' => 'string',
    ];
    protected array $translatable = [
        'contact_title'
    ];
    public function supplierCategories(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(SupplierCategory::class);
    }
    public function products(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Product::class);
    }
    public function inventories(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Inventory::class);
    }
}
