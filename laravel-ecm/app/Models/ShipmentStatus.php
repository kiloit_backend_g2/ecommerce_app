<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShipmentStatus extends Model
{
    use HasFactory;
    protected $fillable = [
        'shipment_id',
        'status_catalog_id',
        'status_time',
        'note',
    ];
    protected $casts = [
        'shipment_id' => 'integer',
        'status_catalog_id' => 'integer',
        'status_time' => 'datetime:Y-m-d H:i:s',
        'note' => 'string',
    ];
    protected array $translatable = [];
    public function shipment(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Shipment::class);
    }
    public function statusCatalog(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(StatusCatalog::class);
    }
}
