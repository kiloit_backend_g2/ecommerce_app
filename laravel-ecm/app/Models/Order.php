<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @method static inRandomOrder()
 */
class Order extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = [
        'date',
        'total_amount',
        'status',
        'users_id',
        'payment_type_id'
    ];
    protected $casts = [
        'date' => 'date:Y-m-d',
        'total_amount' => 'decimal:10,2',
        'status' => 'integer',
        'users_id' => 'integer',
        'payment_type_id' => 'integer',
    ];
    /**
     * @var array
     */
    protected array $translatable = [];
    public function orderItems(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(OrderItem::class);
    }
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function paymentType(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(PaymentType::class);
    }

}



