<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;
    protected $fillable = [
        'product_id',
        'unit_cost',
        'quantity',
        'location',
        'last_restock_date',
        'status',
        'supplier_id',
        'store_id',
    ];
    protected $casts = [
        'product_id' => 'integer',
        'unit_cost' => 'decimal:10,2',
        'quantity' => 'integer',
        'location' => 'string',
        'date' => 'date:Y-m-d',
        'status' => 'integer',
        'supplier_id' => 'integer',
        'store_id' => 'integer'
    ];
    protected array $translatable = [
        'location',
    ];
    public function product(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
    public function supplier(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Supplier::class);
    }
    public function store(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Store::class);
    }
}
