<?php

namespace Database\Factories;

use App\Enums\Transaction\ResponseMessage;
use App\Enums\Transaction\Status;
use App\Models\PaymentType;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Transaction>
 */
class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'uuid' => fake()->unique()->uuid(),
            'payment_type_id' => PaymentType::inRandomOrder()->first()->id,
            'date' => fake()->date('Y-m-d','now'),
            'total_amount' => fake()->randomFloat(2,0,5000),
            'status' => Status::getRandomValue(),
            'response_message' => ResponseMessage::getRandomValue(),
        ];
    }
}
