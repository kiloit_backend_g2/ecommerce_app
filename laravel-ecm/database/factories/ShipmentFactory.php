<?php

namespace Database\Factories;

use App\Models\PaymentType;
use App\Models\Shipment;
use App\Models\ShipmentType;
use App\Models\Store;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Shipment>
 */
class ShipmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'uuid' => fake()->unique()->uuid(),
            'store_id' => Store::inRandomOrder()->first()->id,
            'shipment_type_id' => ShipmentType::inRandomOrder()->first()->id,
            'payment_type_id' => PaymentType::inRandomOrder()->first()->id,
            'shipping_address' => fake()->address(),
            'billing_address' => fake()->address(),
            'product_price' => fake()->randomFloat(2,0,5000),
            'delivery_cost' => fake()->randomFloat(2,0,100),
            'discount' => fake()->randomFloat(2,0,500),
            'final_amount' => fake()->randomFloat(2,0,10000),
            'user_id' => User::inRandomOrder()->first()->id,
        ];
    }
}
