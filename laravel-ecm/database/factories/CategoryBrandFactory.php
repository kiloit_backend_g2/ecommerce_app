<?php

namespace Database\Factories;

use App\Models\Brand;
use App\Models\Category;
use App\Models\CategoryBrand;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<CategoryBrand>
 */
class CategoryBrandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'brand_id' => Brand::inRandomOrder()->first()->id,
            'category_id' => Category::inRandomOrder()->first()->id,
        ];
    }
}
