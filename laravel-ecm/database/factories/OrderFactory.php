<?php

namespace Database\Factories;

use App\Enums\Order\Status;
use App\Models\Order;
use App\Models\PaymentType;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'uuid' => fake()->unique()->uuid(),
            'date' => fake()->date(),
            'total_amount' => fake()->randomFloat(2,0,5000),
            'status' => Status::getRandomValue(),
            'users_id' => User::inRandomOrder()->first()->id,
            'payment_type_id' => PaymentType::inRandomOrder()->first()->id,
        ];
    }
}
