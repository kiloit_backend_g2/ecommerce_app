<?php

namespace Database\Factories;

use App\Enums\Inventory\Status;
use App\Models\Inventory;
use App\Models\Product;
use App\Models\Store;
use App\Models\Supplier;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Inventory>
 */
class InventoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'product_id'   => Product::inRandomOrder()->first()->id,
            'unit_cost'    => fake()->randomFloat(2,0,1000),
            'quantity'     => fake()->randomNumber(),
            'location'     => fake()->streetAddress(),
            'last_restock_date' => fake()->date('Y-m-d','now'),
            'status'       => Status::getRandomValue(),
            'supplier_id'  => Supplier::inRandomOrder()->first()->id,
            'store_id'     => Store::inRandomOrder()->first()->id,
        ];
    }
}
