<?php

namespace Database\Factories;

use App\Models\Shipment;
use App\Models\ShipmentStatus;
use App\Models\StatusCatalog;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<ShipmentStatus>
 */
class ShipmentStatusFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'shipment_id' => Shipment::inRandomOrder()->first()->id,
            'status_catalog_id' => StatusCatalog::inRandomOrder()->first()->id,
            'status_time' => fake()->date('Y-m-d','now'),
            'note' => fake()->text(200),
        ];
    }
}
