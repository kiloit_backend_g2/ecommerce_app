<?php

namespace Database\Factories;

use App\Models\ShipmentType;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<ShipmentType>
 */
class ShipmentTypeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->unique()->name(),
        ];
    }
}
