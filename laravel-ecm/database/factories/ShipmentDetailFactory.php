<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Shipment;
use App\Models\ShipmentDetail;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<ShipmentDetail>
 */
class ShipmentDetailFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'shipment_id' => Shipment::inRandomOrder()->first()->id,
            'product_id' => Product::inRandomOrder()->first()->id,
            'quantity' => fake()->randomNumber(),
            'amount_per_unit' => fake()->randomFloat(2,0,5000),
            'amount' => fake()->randomFloat(2,0,10000),
        ];
    }
}
