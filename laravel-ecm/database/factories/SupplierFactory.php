<?php

namespace Database\Factories;

use App\Models\Supplier;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Supplier>
 */
class SupplierFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'uuid' => fake()->unique()->uuid(),
            'company_name' => fake()->unique()->name(),
            'contact_name' => fake()->unique()->name(),
            'contact_title' => fake()->text(50),
            'contact_email' => fake()->safeEmail(),
            'address' => fake()->address(),
            'city' => fake()->city(),
            'state' => fake()->citySuffix(),
            'country' => fake()->country(),
            'phone' => fake()->phoneNumber(),
        ];
    }
}
