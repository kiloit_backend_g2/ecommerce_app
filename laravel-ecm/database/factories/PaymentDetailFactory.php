<?php

namespace Database\Factories;

use App\Models\PaymentDetail;
use App\Models\Shipment;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<PaymentDetail>
 */
class PaymentDetailFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'shipment_id' => Shipment::inRandomOrder()->first()->id,
            'transaction_id' => Transaction::inRandomOrder()->first()->id,
        ];
    }
}
