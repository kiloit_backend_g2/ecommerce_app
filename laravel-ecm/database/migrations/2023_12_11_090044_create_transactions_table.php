<?php

use App\Enums\Transaction\ResponseMessage;
use App\Enums\Transaction\Status;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->foreignId('payment_type_id')->constrained('payment_types');
            $table->dateTime('date');
            $table->decimal('total_amount',10,2);
            $table->tinyInteger('status')->default(Status::Pending);
            $table->tinyInteger('response_message')->default(ResponseMessage::Pending);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
