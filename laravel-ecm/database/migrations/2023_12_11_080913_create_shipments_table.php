<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid')->unique();
            $table->foreignId('store_id')->constrained('stores');
            $table->foreignId('shipment_type_id')->constrained('shipment_types');
            $table->foreignId('payment_type_id')->constrained('payment_types');
            $table->text('shipping_address');
            $table->text('billing_address');
            $table->decimal('product_price',10,2);
            $table->decimal('delivery_cost',10,2);
            $table->decimal('discount',10,2);
            $table->decimal('final_amount',10,2);
            $table->foreignId('user_id')->constrained('users');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('shipments');
    }
};
