<?php

namespace Database\Seeders;

use App\Models\StatusCatalog;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StatusCatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        StatusCatalog::factory(4)->create();
    }
}
